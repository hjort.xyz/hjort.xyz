
export default {
  /*
  ** Nuxt rendering mode
  ** See https://nuxtjs.org/api/configuration-mode
  */
  mode: 'universal',
  /*
  ** Nuxt target
  ** See https://nuxtjs.org/api/configuration-target
  */
  target: 'static',
  /*
  ** Headers of the page
  ** See https://nuxtjs.org/api/configuration-head
  */
  head: {
    title: 'Emil Hjort',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Hi! My name is Emil and I\'m a Frontend developer with over 4 years of professional experience.' },
      { name: 'theme-color', content: '#1B5E20' }
    ],
    link: [
      { rel: 'preconnect', href: 'https://fonts.gstatic.com/', crossorigin: 'true' },
      { rel: 'icon', type: 'image/x-icon', href: '/hjort-xyz/favicon.ico' },
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Raleway:wght@300;400;500&display=swap' }
    ]
  },
  /*
  ** Global CSS
  */
  css: [
    '@/assets/main.sass',
  ],
  /*
  ** Plugins to load before mounting the App
  ** https://nuxtjs.org/guide/plugins
  */
  plugins: [
  ],
  /*
  ** Auto import components
  ** See https://nuxtjs.org/api/configuration-components
  */
  components: true,
  /*
  ** Nuxt.js dev-modules
  */
  buildModules: [],
  /*
  ** Nuxt.js modules
  */
  modules: [
    '@nuxtjs/pwa',
  ],
  /*
  ** Build configuration
  ** See https://nuxtjs.org/api/configuration-build/
  */
  build: {
  },
  /*
  ** PWA module
  */
  pwa: {
    meta: {
      name: 'Emil Hjort',
      author: 'Emil Oehlenschlæger Hjort',
      description: 'CV for Emil Hjort',
      theme_color: '#1B5E20',
      lang: 'en',
      ogSiteName: 'Emil Hjort',
      ogTitle: 'Emil Hjort',
      ogDescription: 'CV for Emil Hjort'
    },
    manifest: {
      name: 'Emil Hjort',
      short_name: 'Emil Hjort',
      theme_color: '#1B5E20',
      display: 'standalone',
      description: 'CV for Emil Hjort'
    },
    icon: {
      sizes: [64, 120, 144, 152, 192]
    }
  }
}
